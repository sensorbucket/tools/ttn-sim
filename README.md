# ttn-sim

Sends TTN messages to a specified HTTP endpoint

Build the project:
```
go build -o ttn-sim
```

Run the project and specify endpoint:
```
./ttn-sim http://localhost:8080/process
```