package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"time"
)

func main() {

	// Example: "http://localhost:8080/process"
	endpoint := os.Args[1]

	// Parse the ttn messages
	jsonFile, err := os.Open("uplink_message.json")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Successfully Opened uplink_message.json")
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var data []Uplink
	json.Unmarshal(byteValue, &data)

	dataLength := len(data)

	// Sent post requests
	url := endpoint
	fmt.Println("URL:>", url)

	for {
		func() {
			defer time.Sleep(time.Millisecond * 500)
			fmt.Println("up")
			i := rand.Intn(dataLength)
			var jsonStr, _ = json.Marshal(data[i])
			req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
			req.Header.Set("X-Custom-Header", "myvalue")
			req.Header.Set("Content-Type", "application/json")

			client := &http.Client{}
			resp, err := client.Do(req)
			if err != nil {
				fmt.Println(err)
				return
			}
			defer resp.Body.Close()

			fmt.Println("response Status:", resp.Status)
			fmt.Println("response Headers:", resp.Header)
			body, _ := ioutil.ReadAll(resp.Body)
			fmt.Println("response Body:", string(body))
		}()

	}

}

type Uplink struct {
	Result interface{}
}
